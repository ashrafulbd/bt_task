from django.urls import path, include
from rest_framework.routers import SimpleRouter
from rest_framework_jwt.views import obtain_jwt_token

from .views import PeopleAddView, PeopleViewSet

routerPeople = SimpleRouter()
routerPeople.register('', PeopleViewSet)

urlpatterns = [
    path('peopleAdd/', PeopleAddView.as_view()),
    path('people/', include(routerPeople.urls)),
    path('token/', obtain_jwt_token),

]