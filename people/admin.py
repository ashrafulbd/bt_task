from django.contrib import admin

# Register your models here.
from people.models import People, Picture

admin.site.register(People)
admin.site.register(Picture)