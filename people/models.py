from django.db import models
# from django.contrib.postgres.fields import ArrayField


from .randomGet import *

# Create your models here.


class Picture(models.Model):
    large = models.URLField(blank=True, null=True)
    medium = models.URLField(blank=True, null=True)
    thumbnail = models.URLField(blank=True, null=True)

    def __str__(self):
        return str(self.pk)


class People(models.Model):

    name = models.CharField(max_length=100, blank=False, null=False)
    gender = models.CharField(max_length=10, blank=False, null=False)
    city = models.CharField(max_length=50, blank=False, null=False)
    state = models.CharField(max_length=50, blank=False, null=False)
    country = models.CharField(max_length=50, blank=False, null=False)
    postCode = models.CharField(max_length=10, blank=False, null=False)
    email = models.EmailField(blank=False, null=False)
    dob = models.DateTimeField(blank=False, null=False)
    phone = models.CharField(max_length=15, blank=False, null=False)
    # picture = ArrayField(models.URLField(blank=True, null=True))

    picture = models.OneToOneField(Picture, on_delete=models.CASCADE)

    def __str__(self):
        return self.name
