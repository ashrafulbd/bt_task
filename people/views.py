from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.views import APIView

from people.models import People, Picture
from people.randomGet import _get_people
from people.serializers import PeopleSerializer


class PeopleAddView(APIView):
    def get(self, request):
        allPeople = _get_people(100)
        for temp in allPeople:
            # temp = i
            # rand_data = {}
            # print(type(temp))
            rand_data = {
                'name': temp['results'][0]['name']['title'] + ' ' + temp['results'][0]['name']['first'] + ' ' +
                        temp['results'][0]['name']['last'],
                'gender': temp['results'][0]['gender'],
                'city': temp['results'][0]['location']['city'],
                'state': temp['results'][0]['location']['state'],
                'country': temp['results'][0]['location']['country'],
                'postCode': temp['results'][0]['location']['postcode'],
                'email': temp['results'][0]['email'],
                'dob': temp['results'][0]['dob']['date'],
                'phone': temp['results'][0]['phone'],
            }
            picture = {
                'large': temp['results'][0]['picture']['large'],
                'medium': temp['results'][0]['picture']['medium'],
                'thumbnail': temp['results'][0]['picture']['thumbnail'],
            }
            # print(rand_data)
            p = Picture(**picture)
            p.save()
            m = People(picture=p, **rand_data)
            m.save()

            print(str(allPeople.index(temp)+1) + "th person added!")

        return Response('100 people added')



class PeopleViewSet(viewsets.ModelViewSet):
    serializer_class = PeopleSerializer
    queryset = People.objects.all()
    lookup_field = 'pk'
    http_method_names = ['get', ]