import requests


def _get_people(amount):

    url = 'https://randomuser.me/api/'

    data = []

    for i in range(0, amount):
        raw_data = requests.get(url).json()
        # print(raw_data['results'])
        data.append(raw_data)
        print(str(i+1) + "th user fetched!")

    return data


# print(_get_people(2))


#
# temp = {"results":[{"gender":"male","name":{"title":"Mr","first":"Donald","last":"Wheeler"},"location":{"street":{"number":1565,"name":"Railroad St"},"city":"Yakima","state":"New Jersey","country":"United States","postcode":24326,"coordinates":{"latitude":"85.4649","longitude":"-150.5323"},"timezone":{"offset":"+3:00","description":"Baghdad, Riyadh, Moscow, St. Petersburg"}},"email":"donald.wheeler@example.com","login":{"uuid":"4f82af92-382e-478f-a15f-aa74ed6a9959","username":"goldenzebra714","password":"bacchus","salt":"mFj5CH9P","md5":"cb68d1a66f88b32e489df5e2aabc6070","sha1":"551979b623d18c9f63381cb6abeeda987874e2a0","sha256":"aae023ab7bef56fd50bf928e2e393bf356ac21c34796ae5e5cced6cb66ec668c"},"dob":{"date":"1980-06-12T04:10:43.614Z","age":39},"registered":{"date":"2010-04-30T07:33:35.002Z","age":9},"phone":"(884)-655-3576","cell":"(735)-067-2796","id":{"name":"SSN","value":"416-77-8998"},"picture":{"large":"https://randomuser.me/api/portraits/men/25.jpg","medium":"https://randomuser.me/api/portraits/med/men/25.jpg","thumbnail":"https://randomuser.me/api/portraits/thumb/men/25.jpg"},"nat":"US"}],"info":{"seed":"ec82e208a17e2601","results":1,"page":1,"version":"1.3"}}
#
#
# rand_data = {}
# rand_data.update({
#     'name': temp['results'][0]['name']['title'] + ' ' + temp['results'][0]['name']['first'] + ' ' + temp['results'][0]['name']['last'],
#     'gender': temp['results'][0]['gender'],
#     'city': temp['results'][0]['location']['city'],
#     'state': temp['results'][0]['location']['state'],
#     'country': temp['results'][0]['location']['country'],
#     'postCode': temp['results'][0]['location']['postcode'],
#     'email': temp['results'][0]['email'],
#     'dob': temp['results'][0]['dob']['date'],
#     'phone': temp['results'][0]['phone'],
#     'picture': {
#         'large': temp['results'][0]['picture']['large'],
#         'medium': temp['results'][0]['picture']['medium'],
#         'thumbnail': temp['results'][0]['picture']['thumbnail'],
#     }
#
# })
# print(rand_data)